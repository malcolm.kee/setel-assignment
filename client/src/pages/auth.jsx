import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { AuthContext } from '../contexts/auth.context';
import { login } from '../services/auth.service';
import { updateItem } from '../utils/local.storage';
import { toast } from 'react-toastify';
import {TOKEN_KEY} from '../utils/constant.util';

const Auth = () => {
    const history = useHistory();
    const {toggleAuth} = useContext(AuthContext);

    const [formData, setFormData] = useState({
        email: '',
        password: ''
    });
    const submitLogin = async () => {
        try {
            let result = await login({
                ...formData
            });
            if (result.status) {
               
                updateItem(TOKEN_KEY, result.data);
                toggleAuth(true);
                history.push('/home');
            } else {
                toast(result.message);
            }
        } catch (error) {
            toast(error.message)
        }
        
    };

    const handleChange = e => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };
    return (
        <>
            <div className="outer">
                <div className="d-flex flex-row justify-content-end m-2">
                    <div>Login</div>
                    <div>Register</div>
                </div>
                <div className="inner">
                    <form>
                        <h3>Log in</h3>

                        <div className="form-group">
                            <label>Email</label>
                            <input type="email" className="form-control" placeholder="Enter email" name='email' onChange={handleChange}/>
                        </div>

                        <div className="form-group">
                            <label>Password</label>
                            <input type="password" className="form-control" placeholder="Enter password" name='password' onChange={handleChange}/>
                        </div>
                        <button type="button" className="btn btn-dark btn-lg btn-block mt-2" onClick={submitLogin}>Sign in</button>
                    </form>
                </div>
            </div>
          
        </>
    );
}

export default Auth;
