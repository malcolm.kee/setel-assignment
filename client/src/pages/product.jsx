import React, { useState, useEffect } from "react";
import ProductItem from "../components/product/product-item.component";
import { toast } from 'react-toastify';
import { getProduct } from "../services/product.service";

const Product = () => {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    getProduct()
      .then((res) => {
        if (res.status) {
          setProducts(res.data);
      } else {
          toast(res.message);
      }
      })
      .catch((err) => {
        toast(err.message);
      });
  }, []);

  return (
    <>
      <div>
        <h2>Product</h2>
        <div
          style={{
            display: "grid",
            gridTemplateColumns: "auto auto auto auto",
            gridGap: 5,
            width: "80%",
            margin: "auto",
            padding: 10,
          }}
        >
          {products &&
            products.map((item, index) => {
              return <ProductItem key={index} dataItem={item} />;
            })}
        </div>
      </div>
    </>
  );
};

export default Product;
