import { Switch } from "react-router-dom";
import Navbar from "../components/navbar/navbar";
import ProtectedRoute from "../components/protected-route/protected-route";
import { homeRoutes } from '../utils/routes';

const Home = () => {

  return (
    <div>
      <Navbar />
      <Switch>
        {homeRoutes.map((item, index) => <ProtectedRoute key={index} path={item.path} component={item.component} />)}
        <ProtectedRoute  path='/home' component={homeRoutes[0].component} />
      </Switch>


    </div>

  );
};

export default Home;
