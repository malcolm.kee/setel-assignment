import { useContext, useState } from "react";
import { CartContext } from "../contexts/cart.context";
// import { OrderContext } from "../contexts/order.context";
import CartItem from '../components/cart/cart-item';
import { Button } from "react-bootstrap";
import OrderModal from '../components/modal/order-modal.component';



const Cart = () => {
  const { cart } = useContext(CartContext);
 

  const [show, setShow] = useState(false);

  const handleOpen = () => {
    setShow(true);
  };

  const handleClose = () => {
    setShow(false);
  };

  return (
    
    <>
      <div className="container border round" style={{height: '90vh', position: 'relative'}}>
        <h1>Cart</h1>   
        <div>
        {cart.length !== 0 ? cart.map((item, index) => <CartItem key={index} item={item} />) : ''}
        </div>
        
        <div className=" col-12 d-flex flex-row justify-content-between align-items-center p-2" style={{position: 'absolute', bottom: 0, right: 0}}>
          <Button className="col-1" onClick={() => {
            handleOpen();
          }}>Order</Button>
          <div style={{color: 'red'}}>total: {cart.reduce((sum, p) => sum + (p.qty * p.price), 0) || 0}$</div>
        </div>
        <OrderModal show={show} handleClose={handleClose} handleOpen={handleOpen}/>
      </div>
    </>
  );
};

export default Cart;
