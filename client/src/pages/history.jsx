import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { getListOrder } from '../services/order.service';
import HistoryItem from '../components/history/history-item';
import { toast } from 'react-toastify';

const History = () => {
    const [orderList, setStateOrderList] = useState([]);



    useEffect(() => {
        getListOrder()
            .then(res => {
                if (res.status) {
                    setStateOrderList(res.data);
                } else {
                    toast(res.message);
                }

            }).catch(err => {
                toast(err.message);
            })

        setInterval(() => {
            getListOrder()
                .then(res => {
                    if (res.status) {
                        setStateOrderList(res.data);
                    } else {
                        toast(res.message);
                    }

                }).catch(err => {
                    toast(err.message);
                })
        }, 5000);

    }, [])



    return (
        <>
            <Container>

                <h1>History</h1>
                {orderList.length !== 0 ? orderList.map((item, index) => <HistoryItem key={index} item={item} />) : ''}
            </Container>
        </>
    );
}

export default History;
