import { Route, Switch } from "react-router-dom";
import "./App.css";
import Auth from "./pages/auth";
import Home from "./pages/home";
import ProtectedRoute from "./components/protected-route/protected-route";
import AuthProvider from "./contexts/auth.context";
import CartProvider from "./contexts/cart.context";
import OrderProvider from "./contexts/order.context";
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "bootstrap/dist/css/bootstrap.min.css";

toast.configure();
function App() {
  return (
    <div className="App">
      <AuthProvider>
        <CartProvider>
          <OrderProvider>
            <Switch>
              <Route index path="/auth" component={Auth} />
              <ProtectedRoute path="/home" component={Home} />
              <ProtectedRoute path="" component={Home} />
            </Switch>
          </OrderProvider>
        </CartProvider>
      </AuthProvider>
    </div>
  );
}

export default App;
