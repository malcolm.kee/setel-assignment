// import { useContext } from "react";
// import { CartContext } from "../../contexts/cart.context";
import { Image } from "react-bootstrap";

const HistoryItem = ({ item }) => {
  return (
    <>
      <div className="d-flex flex-column border pd-2 m-2 rounded col-12 bg-item">
        <div className="d-flex flex-column col-12 align-items-start">
          <h2>{item ? item.state : ""}</h2>
          <h3>{item ? item.address : ""}</h3>
          <h4>{item ? item.phone : ""}</h4>
          <div style={{color: 'red'}}>{item.products.reduce((sum, p) => sum + (p.qty * p.price), 0)}$</div>
        </div>
        {item.products.map((pr, index) => (
          <div className="d-flex flex-row border pd-2 m-2 rounded col-12" key={index}>
            <Image
              src={
                pr
                  ? pr.img
                  : "https://m.media-amazon.com/images/I/81Sxdp0JBLL._AC_UL320_.jpg"
              }
              className="col-3"
              rounded
            ></Image>
            <div className="d-flex flex-column justify-content-start align-items-start col-9 p-2">
              <div className="d-flex flex-row col-12 justify-content-between">
                <h3>{pr ? pr.name : ""}</h3>
                
                <div className="d-flex flex-row col-2 justify-content-around">
                  <div style={{ color: "red" }}>price: {pr ? pr.price : ""}$</div>
                  <div>quantity: {pr ? pr.qty : ""}</div>
                </div>
              </div>
              <div>{pr ? pr.description : ''}</div>
              
            </div>
           
          </div>
        ))}
      </div>
    </>
  );
};

export default HistoryItem;
