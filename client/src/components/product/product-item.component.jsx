import { useContext } from "react";
import { Image, Button } from "react-bootstrap";
import { CartContext } from "../../contexts/cart.context";

const ProductItem = ({ dataItem }) => {
  
  const { img, price, name } = dataItem;
  const { addToCartOrIncrement } = useContext(CartContext);
  return (
    <>
      <div
        className="d-flex flex-column justify-content-between bg-item"
        style={{
          
          padding: 20,
        }}
      >
        <Image
          src={
            img
              ? img
              : "https://cf.shopee.vn/file/394ed88df2bf7211c65b1871147bd7a4_tn"
          }
        />
        <div className="d-flex flex-row justify-content-between">
          <div className="d-flex flex-column justify-content-start align-items-start p-1">
            <div>{name ? name : ""}</div>
            <div style={{ color: "#EB1919" }}>{price ? price : "0"}$</div>
          </div>
          <Button variant="primary" onClick={() => {
            addToCartOrIncrement(dataItem)
          }}>add to cart</Button>
        </div>
      </div>
    </>
  );
};

export default ProductItem;
