import { useContext } from "react";
import { CartContext } from "../../contexts/cart.context";
import { Image } from "react-bootstrap";


const CartItem = ({ item }) => {
  const { price, img, description, name, qty } = item;
  const { addToCartOrIncrement, decrementQty, removeFromCart } = useContext(CartContext);

  const increment = () => {
    addToCartOrIncrement(item);
  };

  const decrement = () => {
    decrementQty(item);
  };

  const removeCartItem = () => {
     
    removeFromCart(item);
  }

  return (
    <>
      <div className="d-flex flex-row border pd-2 m-2 rounded col-12 bg-item">
        <Image
          src={
            img
              ? img
              : "https://m.media-amazon.com/images/I/81Sxdp0JBLL._AC_UL320_.jpg"
          }
          className="col-3"
          rounded
          //onClick={handleOpen}
        ></Image>
        <div className="d-flex flex-column justify-content-between align-items-start col-9 p-2">
          <div className="d-flex flex-row col-12 justify-content-between">
            <h3>{name ? name : ""}</h3>
            <div className="d-flex flex-row col-2 justify-content-around">
              <div style={{color: 'red'}}>{price}$</div>
              <div style={{cursor: 'default'}} onClick={removeCartItem}>x</div>
            </div>
          </div>
          <p>{description ? description : ""}</p>
          <div className="d-flex flex-row align-items-center col-12">
            <input type="button" value="-" onClick={decrement}></input>
            <div>{qty ? qty : ""}</div>
            <input type="button" value="+" onClick={increment}></input>
          </div>
        </div>
      </div>
    </>
  );
};

export default CartItem;
