import { Route, Redirect } from 'react-router-dom';
import {useContext} from 'react';
import {AuthContext} from '../../contexts/auth.context';

const ProtectedRoute = ({path, component: Component, ...rest}) => {
    const {isAuth} = useContext(AuthContext);
    console.log(path)
    return (
        <Route
            {...rest}
            render={(props) => {
                if (path === '') return <Redirect to="/home"/>
                if(isAuth) return <Component {...props} />;
                else return <Redirect to="/auth"/>
            }}
        />
    )
}

export default ProtectedRoute;
