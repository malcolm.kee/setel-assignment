import { useContext, useState } from "react";
import { CartContext } from "../../contexts/cart.context";
import { Modal, Button, Form } from "react-bootstrap";
import { order } from "../../services/order.service";
// import { OrderContext } from "../../contexts/order.context";
import { toast } from "react-toastify";
//import 'bootstrap/dist/css/bootstrap.min.css';

const ModalOrder = ({ show, handleClose }) => {
  const { cart, removeCart } = useContext(CartContext);

  const [formData, setFormData] = useState({
    name: "",
    phone: "",
    address: "",
  });

  const submitOrder = async () => {
    let result = await order({
      ...formData,
      products: cart
        .filter((e) => e.qty)
        .map((e) => ({
          id: e._id,
          qty: e.qty,
        })),
    });
    return result;
  };

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Order</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3">
            <Form.Label>Name</Form.Label>
            <Form.Control
              name="name"
              onChange={handleChange}
              value={formData.name}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label type="tel">Phone</Form.Label>
            <Form.Control
              name="phone"
              onChange={handleChange}
              value={formData.phone}
            />
          </Form.Group>
          <Form.Group className="mb-3">
            <Form.Label type="tel">Address</Form.Label>
            <Form.Control
              name="address"
              onChange={handleChange}
              value={formData.address}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => {
            handleClose();
          }}
        >
          Close
        </Button>
        <Button
          variant="primary"
          onClick={async () => {
            try {
              let result = await submitOrder();
              if (result.status) {
                handleClose();
                removeCart();
                toast("Created order");
              } else {
                toast("Error when you create order");
              }
            } catch (error) {
              toast(error.message);
            }
          }}
        >
          Order
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ModalOrder;
