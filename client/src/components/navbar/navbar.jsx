import { Navbar, Container, Nav, Badge } from "react-bootstrap";
import { Link } from 'react-router-dom';
import { useContext } from "react";
import {CartContext} from '../../contexts/cart.context';

const NavbarComponent = () => {
  const {cart} = useContext(CartContext);
  return (
   
    <>
    <Navbar bg="light" expand="lg">
      <Container>
        {/* <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" /> */}
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link}  to="/home/product">Product</Nav.Link >
            <Nav.Link as={Link} to="/home/cart">Cart <Badge bg="secondary">{cart.length}</Badge></Nav.Link >
            <Nav.Link as={Link} to="/home/history">history</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </>
    
  );
};

export default NavbarComponent;
