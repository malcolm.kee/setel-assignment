import React, {createContext, useState} from 'react';

export const OrderContext = createContext();

const OrderProvider = ({children}) => {
    const [statusAlert, setStatusAlert] = useState(null);

   const toggleStatusAlert = (status) => {
    setStatusAlert(status);
    setTimeout(() => {
        setStatusAlert(null);
    }, 1000);
   }

    const orderDataContext = {
        statusAlert,
        toggleStatusAlert
    }

    return (
      <OrderContext.Provider value={orderDataContext}>
          {children}
      </OrderContext.Provider>
    )
}

export default OrderProvider;
