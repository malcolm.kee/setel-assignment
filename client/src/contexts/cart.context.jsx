import React, { createContext, useState } from "react";
import { CART_KEY } from "../utils/constant.util";
import { getItem, updateItem } from "../utils/local.storage";

export const CartContext = createContext();

const CartProvider = ({ children }) => {
  const cartData = getItem(CART_KEY);
  const [cart, setCart] = useState(cartData ? cartData : []);

  const addToCartOrIncrement = (item) => {
    const itemCheck = cart.find((p) => p._id === item._id);

    if (itemCheck) {
      const newCart = cart.map((p) => {
        if (p._id === item._id) return { ...p, qty: p.qty + 1 };
        else return p;
      });
      setCart(newCart);
      updateItem(CART_KEY, newCart);
    } else {
      const newCart = cart.concat({ ...item, qty: 1 });
      setCart(newCart);
      updateItem(CART_KEY, newCart);
    }
  };

  const removeFromCart = (item) => {
    const newCart = cart.filter((p) => p._id !== item._id);
    setCart(newCart);
    updateItem(CART_KEY, newCart);
  };

  const decrementQty = (item) => {
    const newCart = cart.map((p) => {
      if (p._id === item._id) return { ...p, qty: p.qty <= 1 ? 1 : p.qty - 1 };
      else return p;
    });
    setCart(newCart);
    updateItem(CART_KEY, newCart);
  };

  const removeCart = () => {
    const newCart = [];
    setCart(newCart);
    updateItem(CART_KEY, newCart);
  };

  const cartDataContext = {
    cart,
    addToCartOrIncrement,
    removeFromCart,
    decrementQty,
    removeCart,
  };

  return (
    <CartContext.Provider value={cartDataContext}>
      {children}
    </CartContext.Provider>
  );
};

export default CartProvider;
