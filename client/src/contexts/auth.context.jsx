import React, {createContext, useState} from 'react';
import { getItem } from '../utils/local.storage';
import {TOKEN_KEY} from '../utils/constant.util';

export const AuthContext = createContext();

const AuthProvider = ({children}) => {
    
    const token = getItem(TOKEN_KEY);
    const [isAuth, setAuth] = useState(token ? true : false);
    const toggleAuth = () => {
      setAuth(true);
    }

    const authData = {
      isAuth,
      toggleAuth
    };

    

    return (
      <AuthContext.Provider value={authData}>
        {children}
      </AuthContext.Provider>
    )
}

export default AuthProvider;
