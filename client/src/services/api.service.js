import { ajax } from 'rxjs/ajax';
import { getItem } from '../utils/local.storage';
import { TOKEN_KEY } from '../utils/constant.util';

export function request(data) {
    const headers = {};
    if (getItem(TOKEN_KEY)) {
        headers['Authorization'] = 'Bearer ' + getItem(TOKEN_KEY);
    }
    return ajax({
        ...data,
        headers,
        responseType: 'json'
    });
}
