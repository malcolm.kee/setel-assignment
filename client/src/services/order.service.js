import { request } from './api.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

export async function order(data) {
    return request({
        url: `${process.env.REACT_APP_BASE_API}/order`,
        method: 'POST',
        body: data
    })
        .pipe(
            map(res => {
                console.log(res);
                return res.response;
            }),
            catchError(error => {
                return of(error.response);
            })
        )
        .toPromise();
}

export async function getListOrder() {
    const obs$ = await request({
        url: `${process.env.REACT_APP_BASE_API}/order/all`,
        method: 'GET'
    })
        .pipe(
            map(userResponse => userResponse.response),
            catchError(error => {
                return of(error.response);
            })
        )
        .toPromise();
    return obs$;
}

export async function cancelOrder(id) {
    const obs$ = await request({
        url: `${process.env.REACT_APP_BASE_API}/order/${id}/cancel`,
        method: 'PUT'
    })
        .pipe(
            map(userResponse => userResponse.response),
            catchError(error => {
                return of(error.response);
            })
        )
        .toPromise();
    return obs$;
}
