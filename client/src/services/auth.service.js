import { request } from './api.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

export async function login(data) {
    return request({
        url: `${process.env.REACT_APP_BASE_API}/auth/login`,
        method: 'POST',
        body: data
    })
        .pipe(
            map(res => {
                return res.response;
            }),
            catchError(error => {
                return of(error.response);
            })
        )
        .toPromise();
}

export async function register(data) {
    return request({
        url: `${process.env.REACT_APP_BASE_API}/auth/register`,
        method: 'POST',
        body: data
    })
        .pipe(
            map(res => {
                return res.response;
            }),
            catchError(error => {
                return of(error.response);
            })
        )
        .toPromise();
}
