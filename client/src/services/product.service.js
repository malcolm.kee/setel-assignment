import { request } from './api.service';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

export async function getProduct() {
    const obs$ = await request({
        url: `${process.env.REACT_APP_BASE_API}/product/all`,
        method: 'GET'
    })
        .pipe(
            map(userResponse => userResponse.response),
            catchError(error => {
                return of(error.response);
            })
        )
        .toPromise();
    return obs$;
}
