import Cart from '../pages/cart';
import Product from '../pages/product';
import History from '../pages/history';

export const homeRoutes = [
    {
        path: '/home/product',
        label: 'Product',
        component: Product
    },
    {
        path: '/home/cart',
        label: 'Cart',
        component: Cart
    },
    {
        path: '/home/history',
        label: 'History',
        component: History
    }

]
