export const setItem = (key, value) => {
    value = JSON.stringify(value);
    localStorage.setItem(key, value);
}

export const getItem = (key) => {
    const item  = localStorage.getItem(key);
    if (item) {
        return JSON.parse(item);
    } 
    return null;
}

export const removeItem = (key) => {
   
    localStorage.removeItem(key);
}

export const updateItem = (key, data) => {
    
    const item = localStorage.getItem(key);
    if (item) {
        localStorage.removeItem(key);
        data = JSON.stringify(data);
        localStorage.setItem(key, data);
       
    } else {
        console.log('debug1', key)
        data = JSON.stringify(data);
        localStorage.setItem(key, data);
    }
}
