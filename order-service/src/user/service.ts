import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { hashPassword } from "src/utils/ultil";
import { CreateUserDto } from "./dto/create.user.dto";
import { User, UserDocument } from "./schemas/user.schema";

@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private readonly model: Model<UserDocument>){}

    async create(dto: CreateUserDto): Promise<User> {
        const hash = await hashPassword(dto.password);
        dto.password = hash;
        return this.model.create(dto);
    }

    async findUserByEmail(email: string): Promise<User> {
        return this.model.findOne({email}).exec();
    }
}