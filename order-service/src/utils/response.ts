export class ResponseObject {
    code: number;
    message: string;
    status: boolean;
    data: any;
    constructor(code: number, message: string, status: boolean, data: any){
        this.code = code;
        this.message = message;
        this.status = status;
        this.data = data;
    }
}