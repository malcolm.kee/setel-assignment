import { BadRequestException, Injectable } from '@nestjs/common';
import { UserService } from 'src/user/service';
import { ValidateUserDto } from './dto/validate.user.dto';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from './dto/create.user.dto';
import { User } from 'src/user/schemas/user.schema';
import { comparePassword } from 'src/utils/ultil';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}
  //validate user
  async validateUser(dto: ValidateUserDto): Promise<any> {
    const user: any = await this.userService.findUserByEmail(dto.email);

    if (user) {
      const isMatch = await comparePassword(dto.password, user.password);
      if (isMatch) {
        const { password, ...rest } = user._doc;
        return rest;
      }
      throw new BadRequestException('Password is not correct!');
    }

    return null;
  }

  //verify token
  async verifyToken(email: string): Promise<any> {
    const user: any = await this.userService.findUserByEmail(email);
    if (user) {
      const { password, ...rest } = user._doc;
      return rest;
    }
    return null;
  }

  //create user
  async create(dto: CreateUserDto): Promise<User> {
    return this.userService.create(dto);
  }

  async login(user: any) {
    const payload = { email: user.email, id: user._id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
