import { Controller, Post, UseGuards, Request, Get, Body, ValidationPipe } from '@nestjs/common';
import { CreateUserDto } from 'src/user/dto/create.user.dto';
import { ResponseObject } from 'src/utils/response';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { AuthService } from './service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    const { access_token } = await this.authService.login(req.user);
    
    return new ResponseObject(200, 'Login successfully!', true, access_token)
  }

  @Post('register')
  async create(@Body(new ValidationPipe({ transform: true })) dto: CreateUserDto) {
    const user = await this.authService.create(dto);
    return new ResponseObject(201, 'Created user!', true, user);
  }
}
