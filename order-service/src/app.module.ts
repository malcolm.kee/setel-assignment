import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';
import { RouterModule } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { ProductModule } from './product/module';
import { OrderModule } from './order/module';
import { AuthModule } from './auth/module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGODB_URL),
    ProductModule,
    OrderModule,
    AuthModule,
    RouterModule.register([
      {
        path: '',
        module: OrderModule,
      },
      {
        path: '',
        module: ProductModule,
      },
      {
        path: '',
        module: AuthModule
      }
    ]),

    //UserModule,
  ],
})
export class AppModule {}
