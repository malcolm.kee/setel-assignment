import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product, ProductDocument } from './schemas/product.schema';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product.name)
    private readonly model: Model<ProductDocument>,
  ) {}

  async find(query: any = {}, project: any = {}): Promise<Product[]> {
    return this.model.find(query, project).exec();
  }

  async findOne(query: any = {}): Promise<Product> {
    return this.model.findOne(query).exec();
  }
}
