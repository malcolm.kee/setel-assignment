import { Controller, Get } from '@nestjs/common';
import { ResponseObject } from 'src/utils/response';
import { ProductService } from './service';

@Controller('product')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Get('all')
  async all() {
    const data = await this.productService.find({}, {});
    const successObj = new ResponseObject(200, 'Response order', true, data);
    return successObj;
  }
}
