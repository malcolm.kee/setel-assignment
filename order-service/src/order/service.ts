import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateOrderDto } from './dto/create.order.dto';
import { Order, OrderDocument, OrderStateEnum } from './schemas/order.schema';
import { ProductService } from '../product/service';
import { HttpService } from '@nestjs/axios';
import { map } from 'rxjs';

@Injectable()
export class OrderService {
  constructor(
    @InjectModel(Order.name)
    private readonly model: Model<Order>,
    private readonly productService: ProductService,
    private readonly httpService: HttpService,
  ) {}

  // get all order
  async getAll(userId: any): Promise<any[]> {
    console.log('get list order');
    const orders = await this.model.find({userId: userId}).exec();
    const productIds = new Set(
      orders.reduce(
        (acc, curr) => acc.concat(curr.products.map((e) => e.id)),
        [],
      ),
    );

    const products = await this.productService.find({
      _id: {
        $in: [...productIds],
      },
    });
  
    return orders.map((order) => ({
      ...order.toJSON(),
      products: order.products.map((pr: any) => ({
        ...pr.toJSON(),
        name: products.find((e) => e._id.toHexString() === pr.id.toHexString())
          .name,
          img: products.find((e) => e._id.toHexString() === pr.id.toHexString()).img,
          description: products.find((e) => e._id.toHexString() === pr.id.toHexString()).description
      })),
    }));
  }

  //create order
  async create(dto: CreateOrderDto, user: any) {
    console.log('create order');

    const productIds = dto.products.map((e) => e.id);
    const products = await this.productService.find(
      {
        _id: {
          $in: productIds,
        },
      },
      {
        price: 1,
      },
    );

    if (!products || products.length !== productIds.length) {
      throw new BadRequestException();
    }

    const order = await this.model.create({
      ...dto,
      products: dto.products.map((e) => ({
        ...e,
        price: products.find((p) => e.id === p._id.toHexString()).price,
      })),
      userId: user._id,
    });

    // call payment service
    const paymentResult = await this.httpService
      .post(
        'processPayment',
        {
          userId: user._id,
          amount: order.products.reduce(
            (acc, curr) => acc + curr.price * curr.qty,
            0,
          ),
          pin: '123456',
        },
        {
          baseURL: process.env.PAYMENT_URL,
        },
      )
      .pipe(map((response) => response.data))
      .toPromise();
    order.state =
      paymentResult && paymentResult.success
        ? OrderStateEnum.CONFIRMED
        : OrderStateEnum.CANCELLED;
    await this.updateState(order._id, order.state);

    if (order.state === OrderStateEnum.CONFIRMED) {
      setTimeout(() => {
        this.updateState(order._id, OrderStateEnum.DELIVERED);
      }, 10000);
    }

    return order;
  }

  //update status order
  async updateState(id: string, state: OrderStateEnum) {
    console.log('update status order');
    return await this.model.updateOne(
      {
        _id: id,
        state: {
          $ne: OrderStateEnum.DELIVERED,
        },
      },
      {
        $set: {
          state,
        },
      },
    );
  }
}
