import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type OrderDocument = Order & Document;
export enum OrderStateEnum {
  CREATED = 'CREATED',
  CONFIRMED = 'CONFIRMED',
  CANCELLED = 'CANCELLED',
  DELIVERED = 'DELIVERED',
}

@Schema()
export class Order {
  @Prop({ required: true })
  userId: Types.ObjectId;

  @Prop({ required: true })
  address: string;

  @Prop({ required: true })
  phone: string;

  @Prop({
    type: String,
    enum: Object.values(OrderStateEnum),
    required: true,
    default: OrderStateEnum.CREATED,
  })
  state: OrderStateEnum;

  @Prop({
    type: [
      {
        _id: false,
        id: Types.ObjectId,
        qty: Number,
        price: Number,
      },
    ],
    required: true,
  })
  products: {
    id: Types.ObjectId;
    qty: number;
    price: number;
  }[];

  @Prop({ default: () => new Date() })
  createdAt: Date;

  @Prop({ default: () => new Date() })
  updatedAt: Date;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
