import { OrderStateEnum } from './schemas/order.schema';
import { Types } from 'mongoose';
import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  ValidationPipe,
  Param,
  UseGuards,
  Request
} from '@nestjs/common';;
import { CreateOrderDto } from './dto/create.order.dto';
import { OrderService } from './service';
import { ResponseObject } from 'src/utils/response';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Get('all')
  async all(@Request() req) {
    const {_id} = req.user;
  
    const data = await this.orderService.getAll(_id);
    const successObj = new ResponseObject(200, 'Response Order', true, data);
    return successObj;
  }

  @Post()
  async create(
    @Request() req,
    @Body(new ValidationPipe({ transform: true })) dto: CreateOrderDto,
  ) { 
    const user = req.user;
    const result = await this.orderService.create(dto, user);

    return new ResponseObject(201, 'Response Order', true, result);
  }

  @Put(':id/cancel')
  async cancel(@Param('id') id: string) {
    const result = await this.orderService.updateState(
      id,
      OrderStateEnum.CANCELLED,
    );

    return new ResponseObject(201, 'Response Order', true, result.matchedCount > 0);
  }
}
