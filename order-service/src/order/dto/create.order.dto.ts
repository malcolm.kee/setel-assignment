import { ApiProperty } from '@nestjs/swagger';

export class CreateOrderDto {
  @ApiProperty({
    isArray: true,
  })
  products: {
    id: string;
    qty: number;
  }[];

  @ApiProperty()
  phone: string;
  @ApiProperty()
  address: string;
}
