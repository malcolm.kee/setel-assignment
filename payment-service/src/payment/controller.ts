import { Body, Controller, Post } from '@nestjs/common';
import { RequestPaymentDto } from './dto/request.payment.dto';
import { PaymentService } from './service';

@Controller('payment')
export class PaymentController {
  constructor(private paymentService: PaymentService) {}

  @Post('processPayment')
  processPayment(@Body() dto: RequestPaymentDto) {
    return this.paymentService.processPayment(dto);
  }
}
