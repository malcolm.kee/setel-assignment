export class RequestPaymentDto {
  userId: string;
  amount: number;
  pin: string;
}
