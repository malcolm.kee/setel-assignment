import { Injectable } from '@nestjs/common';
import { RequestPaymentDto } from './dto/request.payment.dto';

@Injectable()
export class PaymentService {
  async processPayment(dto: RequestPaymentDto) {
    if (!dto.pin || dto.pin !== '123456' || dto.amount <= 0) {
      return { success: false };
    }

    return { success: Math.random() > 0.5 };
  }
}
