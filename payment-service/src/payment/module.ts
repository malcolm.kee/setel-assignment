import { PaymentController } from './controller';
import { Module } from '@nestjs/common';
import { PaymentService } from './service';

@Module({
  imports: [],
  controllers: [PaymentController],
  providers: [PaymentService],
  exports: [],
})
export class PaymentModule {}
