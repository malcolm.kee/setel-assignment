import { PaymentModule } from './payment/module';
import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';

@Module({
  imports: [
    PaymentModule,
    RouterModule.register([
      {
        path: '',
        module: PaymentModule,
      },
    ]),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
